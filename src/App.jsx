import Header from "./components/Header";
import "./App.css";
import Acceuil from "./pages/Acceuil";
import CV from "./pages/CV";
import Portfolio from "./pages/Portfolio";

import { useState } from "react";

export default function App() {
  const [pageCourrante, setPageCourrante] = useState("acceuil");

  const changePage = (page) => {
    return () => {
      setPageCourrante(page);
    };
  };

  return (
    <>
      <Header changePage={changePage} />

      {pageCourrante === "acceuil" && <Acceuil/>}

      {pageCourrante === "CV" && <CV/>}

      {pageCourrante === "Portfolio" && <Portfolio/>}

      
    </>
  );
}
