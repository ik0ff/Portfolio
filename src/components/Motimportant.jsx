import style from "./Motimportant.module.css";

export default function Motimportant(props) {
  return <span className={style.important}>{props.children}</span>;
}
