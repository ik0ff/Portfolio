import style from "./ParagraphContainner.module.css";

export default function ParagraphContainner(props) {
  return <div className={style.borderDefault}>{props.children}</div>;
}
