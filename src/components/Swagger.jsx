import style from "./Swagger.module.css";

export default function Swagger(props) {
  return (
    <span id={props.id} className={style.swag}>
      {props.children}
    </span>
  );
}
