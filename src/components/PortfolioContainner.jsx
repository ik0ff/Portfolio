import style from "./PortfolioContainner.module.css";
import Swagger from "./Swagger";
import { Card } from "react-bootstrap";
import Lorem from "./Lorem.jsx";
import ParagraphContainner from "./ParagraphContainner.jsx";
import FantasyFont from "./FantasyFont";
export default function ConContainner() {
  return (
    <div>
      <div className={style.containner}>
        <div className={style.Pheader}>MES PROJETS </div>
        <div className={style.Firstrow}>
          {/* Les cartes viennent de bootstrap ,il s'agit de containner 
          avec un css déja implémenter.
          je plannifie mettre un background image 
          du projet en question , les liens sont self explainatory.*/}

          <Card className={style.card} style={{ width: "16rem" }}>
            <Card.Body className={style.Body}>
              <Card.Title>Menu Café </Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                Gestionnaire de projet
              </Card.Subtitle>
              <Card.Text>-------------------</Card.Text>
              <Card.Link href="#projet1">
                Voir la description du projet{" "}
              </Card.Link>
              <Card.Link href="https://github.com/ik0ff/Project-Cafe.git">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                >
                  <path d="M12 0c-6.626 0-12 5.373-12 12 0 5.302 3.438 9.8 8.207 11.387.599.111.793-.261.793-.577v-2.234c-3.338.726-4.033-1.416-4.033-1.416-.546-1.387-1.333-1.756-1.333-1.756-1.089-.745.083-.729.083-.729 1.205.084 1.839 1.237 1.839 1.237 1.07 1.834 2.807 1.304 3.492.997.107-.775.418-1.305.762-1.604-2.665-.305-5.467-1.334-5.467-5.931 0-1.311.469-2.381 1.236-3.221-.124-.303-.535-1.524.117-3.176 0 0 1.008-.322 3.301 1.23.957-.266 1.983-.399 3.003-.404 1.02.005 2.047.138 3.006.404 2.291-1.552 3.297-1.23 3.297-1.23.653 1.653.242 2.874.118 3.176.77.84 1.235 1.911 1.235 3.221 0 4.609-2.807 5.624-5.479 5.921.43.372.823 1.102.823 2.222v3.293c0 .319.192.694.801.576 4.765-1.589 8.199-6.086 8.199-11.386 0-6.627-5.373-12-12-12z" />
                </svg>
              </Card.Link>
            </Card.Body>
          </Card>

          <Card style={{ width: "16rem" }}>
            <Card.Body>
              <Card.Title>Jeu de mémoire</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                Gestionnaire de projet
              </Card.Subtitle>
              <Card.Text>-------------------</Card.Text>
              <Card.Link href="#projet2">
                Voir la description du projet
              </Card.Link>
              <Card.Link href="https://github.com/ik0ff/Jeu-de-memoire.git">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                >
                  <path d="M12 0c-6.626 0-12 5.373-12 12 0 5.302 3.438 9.8 8.207 11.387.599.111.793-.261.793-.577v-2.234c-3.338.726-4.033-1.416-4.033-1.416-.546-1.387-1.333-1.756-1.333-1.756-1.089-.745.083-.729.083-.729 1.205.084 1.839 1.237 1.839 1.237 1.07 1.834 2.807 1.304 3.492.997.107-.775.418-1.305.762-1.604-2.665-.305-5.467-1.334-5.467-5.931 0-1.311.469-2.381 1.236-3.221-.124-.303-.535-1.524.117-3.176 0 0 1.008-.322 3.301 1.23.957-.266 1.983-.399 3.003-.404 1.02.005 2.047.138 3.006.404 2.291-1.552 3.297-1.23 3.297-1.23.653 1.653.242 2.874.118 3.176.77.84 1.235 1.911 1.235 3.221 0 4.609-2.807 5.624-5.479 5.921.43.372.823 1.102.823 2.222v3.293c0 .319.192.694.801.576 4.765-1.589 8.199-6.086 8.199-11.386 0-6.627-5.373-12-12-12z" />
                </svg>
              </Card.Link>
            </Card.Body>
          </Card>
        </div>

        {/*j'ai créé un deuxieme containner 
          pour pouvoir avoir un effet différent pour la 
          deuxieme rangée de carte Projet */}

        <div className={style.secondRow}>
          <Card style={{ width: "12rem" }}>
            <Card.Body>
              <Card.Title>Projet 3</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                Card Subtitle
              </Card.Subtitle>
              <Card.Text>-------------------</Card.Text>
              <Card.Link href="#projet3">Anchor Link</Card.Link>
              <Card.Link href="#">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                >
                  <path d="M12 0c-6.626 0-12 5.373-12 12 0 5.302 3.438 9.8 8.207 11.387.599.111.793-.261.793-.577v-2.234c-3.338.726-4.033-1.416-4.033-1.416-.546-1.387-1.333-1.756-1.333-1.756-1.089-.745.083-.729.083-.729 1.205.084 1.839 1.237 1.839 1.237 1.07 1.834 2.807 1.304 3.492.997.107-.775.418-1.305.762-1.604-2.665-.305-5.467-1.334-5.467-5.931 0-1.311.469-2.381 1.236-3.221-.124-.303-.535-1.524.117-3.176 0 0 1.008-.322 3.301 1.23.957-.266 1.983-.399 3.003-.404 1.02.005 2.047.138 3.006.404 2.291-1.552 3.297-1.23 3.297-1.23.653 1.653.242 2.874.118 3.176.77.84 1.235 1.911 1.235 3.221 0 4.609-2.807 5.624-5.479 5.921.43.372.823 1.102.823 2.222v3.293c0 .319.192.694.801.576 4.765-1.589 8.199-6.086 8.199-11.386 0-6.627-5.373-12-12-12z" />
                </svg>
              </Card.Link>
            </Card.Body>
          </Card>

          <Card style={{ width: "12rem" }}>
            <Card.Body>
              <Card.Title>Projet 4</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                Card Subtitle
              </Card.Subtitle>
              <Card.Text>-------------------</Card.Text>
              <Card.Link href="#projet4">Anchor Link</Card.Link>
              <Card.Link href="#">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                >
                  <path d="M12 0c-6.626 0-12 5.373-12 12 0 5.302 3.438 9.8 8.207 11.387.599.111.793-.261.793-.577v-2.234c-3.338.726-4.033-1.416-4.033-1.416-.546-1.387-1.333-1.756-1.333-1.756-1.089-.745.083-.729.083-.729 1.205.084 1.839 1.237 1.839 1.237 1.07 1.834 2.807 1.304 3.492.997.107-.775.418-1.305.762-1.604-2.665-.305-5.467-1.334-5.467-5.931 0-1.311.469-2.381 1.236-3.221-.124-.303-.535-1.524.117-3.176 0 0 1.008-.322 3.301 1.23.957-.266 1.983-.399 3.003-.404 1.02.005 2.047.138 3.006.404 2.291-1.552 3.297-1.23 3.297-1.23.653 1.653.242 2.874.118 3.176.77.84 1.235 1.911 1.235 3.221 0 4.609-2.807 5.624-5.479 5.921.43.372.823 1.102.823 2.222v3.293c0 .319.192.694.801.576 4.765-1.589 8.199-6.086 8.199-11.386 0-6.627-5.373-12-12-12z" />
                </svg>
              </Card.Link>
            </Card.Body>
          </Card>

          <Card style={{ width: "12rem" }}>
            <Card.Body>
              <Card.Title>Projet 5 </Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                Card Subtitle
              </Card.Subtitle>
              <Card.Text>-------------------</Card.Text>
              <Card.Link href="#projet5">Anchor Link</Card.Link>
              <Card.Link href="#">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                >
                  <path d="M12 0c-6.626 0-12 5.373-12 12 0 5.302 3.438 9.8 8.207 11.387.599.111.793-.261.793-.577v-2.234c-3.338.726-4.033-1.416-4.033-1.416-.546-1.387-1.333-1.756-1.333-1.756-1.089-.745.083-.729.083-.729 1.205.084 1.839 1.237 1.839 1.237 1.07 1.834 2.807 1.304 3.492.997.107-.775.418-1.305.762-1.604-2.665-.305-5.467-1.334-5.467-5.931 0-1.311.469-2.381 1.236-3.221-.124-.303-.535-1.524.117-3.176 0 0 1.008-.322 3.301 1.23.957-.266 1.983-.399 3.003-.404 1.02.005 2.047.138 3.006.404 2.291-1.552 3.297-1.23 3.297-1.23.653 1.653.242 2.874.118 3.176.77.84 1.235 1.911 1.235 3.221 0 4.609-2.807 5.624-5.479 5.921.43.372.823 1.102.823 2.222v3.293c0 .319.192.694.801.576 4.765-1.589 8.199-6.086 8.199-11.386 0-6.627-5.373-12-12-12z" />
                </svg>
              </Card.Link>
            </Card.Body>
          </Card>
        </div>
      </div>

      <h3>Définitions des projets</h3>

      <Swagger id="projet1">Menu Café &#9749;</Swagger>
      <ParagraphContainner>
        <FantasyFont>
          Le projet menu café a été fait dans un cadre académique.Il s'agit d'un
          site web qui a pour but , comme le nom le suggère d'être le menu d'un
          café. Nous devions apporter des validations serveurs et client sur un
          gabarit présenté par le professeur
        </FantasyFont>
      </ParagraphContainner>

      <Swagger id="projet2">Jeu de mémoire &#10067; </Swagger>
      <ParagraphContainner>
        <FantasyFont>
          Un jeu de mémoire conçut a ma deuxieme session à la Cité. Nous devions
          concevoir un site web intéractif pour l'utilisateur. Comme le nom
          mentionne , il s'agit simplement de trouver les images identiques (2)
          dans le lot
        </FantasyFont>
      </ParagraphContainner>

      <Swagger id="projet3">Projet 3 </Swagger>
      <ParagraphContainner>
        <Lorem />
      </ParagraphContainner>
      <Swagger id="projet4">Projet 4 </Swagger>
      <ParagraphContainner>
        <Lorem />
      </ParagraphContainner>
      <Swagger id="projet5">Projet 5 </Swagger>
      <ParagraphContainner>
        <Lorem />
      </ParagraphContainner>
    </div>
  );
}
