import style from "./MainMenu.module.css";
import ReactTooltip from "react-tooltip";

export default function MainMenu(props) {
  return (
    <nav className={style.Nav}>
      <div className={style.pageOption}>
        <a href="/#" onClick={props.changePage("acceuil")} data-tip="Acceuil">
          <ReactTooltip />
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="25"
            height="18"
            viewBox="0 0 24 24"
          >
            <path d="M21 13v10h-6v-6h-6v6h-6v-10h-3l12-12 12 12h-3zm-1-5.907v-5.093h-3v2.093l3 3z" />
          </svg>
        </a>
      </div>

      <div className={style.pageOption}>
        <a href="/#" onClick={props.changePage("CV")} data-tip="CV">
          <ReactTooltip />

          <svg
            width="25"
            height="18"
            xmlns="http://www.w3.org/2000/svg"
            fill-rule="evenodd"
            clip-rule="evenodd"
          >
            <path d="M3 24h19v-23h-1v22h-18v1zm17-24h-18v22h18v-22zm-3 17h-12v1h12v-1zm0-3h-12v1h12v-1zm0-3h-12v1h12v-1zm-7.348-3.863l.948.3c-.145.529-.387.922-.725 1.178-.338.257-.767.385-1.287.385-.643 0-1.171-.22-1.585-.659-.414-.439-.621-1.04-.621-1.802 0-.806.208-1.432.624-1.878.416-.446.963-.669 1.642-.669.592 0 1.073.175 1.443.525.221.207.386.505.496.892l-.968.231c-.057-.251-.177-.449-.358-.594-.182-.146-.403-.218-.663-.218-.359 0-.65.129-.874.386-.223.258-.335.675-.335 1.252 0 .613.11 1.049.331 1.308.22.26.506.39.858.39.26 0 .484-.082.671-.248.187-.165.322-.425.403-.779zm3.023 1.78l-1.731-4.842h1.06l1.226 3.584 1.186-3.584h1.037l-1.734 4.842h-1.044z" />
          </svg>
        </a>
      </div>

      <div className={style.pageOption}>
        <a
          href="/#"
          onClick={props.changePage("Portfolio")}
          data-tip="Portfolio"
        >
          <ReactTooltip />

          {
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="25"
              height="18"
              viewBox="0 -2 24 24"
            >
              <path d="M12.23 15.5c-6.801 0-10.367-1.221-12.23-2.597v9.097h24v-8.949c-3.218 2.221-9.422 2.449-11.77 2.449zm1.77 2.532c0 1.087-.896 1.968-2 1.968s-2-.881-2-1.968v-1.032h4v1.032zm-14-8.541v-2.491h24v2.605c0 5.289-24 5.133-24-.114zm9-7.491c-1.104 0-2 .896-2 2v2h2v-1.5c0-.276.224-.5.5-.5h5c.276 0 .5.224.5.5v1.5h2v-2c0-1.104-.896-2-2-2h-6z" />
            </svg>
          }
        </a>
      </div>
    </nav>
  );
}
