import style from "./Header.module.css";
import ConTog from "./ContentToggler";
import MainMenu from "./MainMenu";

import MyTittle from "./MyTitles";

export default function Header(props) {
  return (
    <header className={style.header}>
      <div>
        <h1>Krystopher Gendron-Tierney </h1>
        <MyTittle />
      </div>
      <div>
        <ConTog className={style.contog}>
          <MainMenu changePage={props.changePage} />{" "}
        </ConTog>
      </div>
    </header>
  );
}
