import { useState } from "react";
import style from "./ContentToggler.module.css";

export default function ConTog(props) {
  const [visible, setVisible] = useState(false);

  const TogCon = () => {
    setVisible(!visible);
  };

  return (
    <>
      <button className={style.toggle} onClick={TogCon}>
        {
          <svg
            //className={visible ? setVisible : false}
            xmlns="http://www.w3.org/2000/svg"
            width="15"
            height="20"
            viewBox="0 0 24 24"
          >
            <path d="M4 22h-4v-4h4v4zm0-12h-4v4h4v-4zm0-8h-4v4h4v-4zm3 0v4h17v-4h-17zm0 12h17v-4h-17v4zm0 8h17v-4h-17v4z" />
          </svg>
        }
      </button>

      {visible && <div>{props.children}</div>}
    </>
  );
}
