import style from "./MyTittles.module.css";

export default function MyTittle() {
  return (
    <div className={style.titres}>
      <div className={style.itemWrapper}>
        <div className={style.scrollItem}>&#9997; Étudiant</div>
        <div className={style.scrollItem}>&#128187;Programmeur</div>
        <div className={style.scrollItem}>&#127918;Gamer </div>
        <div className={style.scrollItem}>&#9749;Coffee amateur</div>
        <div className={style.scrollItem}>&#127821; #HawaïennePizzaISLife</div>
      </div>
    </div>
  );
}
