import style from "./FantasyFont.module.css";

export default function FantasyFont(props) {
  return <span className={style.Fantasy}>{props.children}</span>;
}
