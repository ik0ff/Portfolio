import style from "./Lorem.module.css";

export default function Lorem() {
  return (
    <div className={style.lorem}>
      Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tenetur ad hic
      possimus, vero est veritatis perferendis ratione repellendus voluptates,
      voluptatum eaque dolor maiores repellat, esse architecto autem temporibus
      quas quia.
    </div>
  );
}
