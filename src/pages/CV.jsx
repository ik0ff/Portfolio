import style from "../components/CV.module.css";
import ParagraphContainner from "../components/ParagraphContainner";
import Swagger from "../components/Swagger";

export default function CV() {
  return (
    <>
      <div className={style.body}>
        <Swagger>Compétences informatiques </Swagger>
        <ParagraphContainner>
          <li>
            {" "}
            Connaissances dans plusieurs langages variées : C#,
            Java,JavaScript,Html, CSS{" "}
          </li>
          <li>
            Connaissances en gestion de bases de données : SQL,
            MongoDB,Cassandra
          </li>
          <li>Connaissances des systèmes d’exploitation Windows et Linux </li>
          <li>
            Connaissance en développement d’application Mobile : Android et iOS
          </li>
        </ParagraphContainner>

        <Swagger>Formation & Certification </Swagger>
        <ParagraphContainner>
          <li>
            {" "}
            Programmation informatique -Cité collégiale --date de fin Déc. 2022
            <span className={style.special}>
              *Gestionnaire de plusieurs projets de synthèse
            </span>
          </li>

          <li>Certificat cisco - Introduction to Networks </li>
          <li>
            D.E.S -Polyvalente de l’Érablière --Juin 2009
            <span className={style.special}>
              *3 années en « Cisco networking » avec le programme P.M.I
            </span>
          </li>
        </ParagraphContainner>

        <Swagger>Expérience de Travail</Swagger>
        <ParagraphContainner>
          <li>
            {" "}
            Agent de programme (PM-01) - Santé Canada -- Jan. 2017 - Mai 2017
          </li>
        </ParagraphContainner>
        <Swagger>Aptitudes et compétences</Swagger>
        <ParagraphContainner>
          <li> Facilité au travail autonome </li>
          <li> Intérêt et expérience à la gestion</li>
          <li> Plusieurs années d’expérience avec le service à la clientèle</li>
          <li>Bilingue : Anglais, Français</li>
        </ParagraphContainner>
      </div>
    </>
  );
}
